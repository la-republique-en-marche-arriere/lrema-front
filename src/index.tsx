import React from "react";
import ReactDOM from "react-dom";
import { hot } from "react-hot-loader";
import { create } from "jss";
import JssProvider from "react-jss/lib/JssProvider";
import CssBaseLine from "@material-ui/core/CssBaseline";
import {
  createGenerateClassName,
  jssPreset,
  MuiThemeProvider
} from "@material-ui/core/styles";
import theme from "./theme";
import App from "./App";
import { apolloClient } from "./api/graphql";
import { ApolloProvider } from "react-apollo";

const generateClassName = createGenerateClassName();
const jss = create({
  ...jssPreset(),
  insertionPoint: "jss-insertion-point"
});

const HotModule = hot(module)(() => (
  <ApolloProvider client={apolloClient}>
    <JssProvider jss={jss} generateClassName={generateClassName}>
      <MuiThemeProvider theme={theme}>
        <CssBaseLine />
        <App />
      </MuiThemeProvider>
    </JssProvider>
  </ApolloProvider>
));

ReactDOM.render(<HotModule />, window.document.getElementById("app"));
