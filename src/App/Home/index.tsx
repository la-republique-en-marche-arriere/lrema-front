import React from "react";
import { Query } from "react-apollo";
import query from "./query";
import FactCard from "../../components/FactCard";
import { Projet } from "../../domain/Projet";

interface Data {
  projets: Projet[];
}

export const Home = () => (
  <Query<Data> query={query}>
    {({ loading, error, data }) => {
      if (loading || !data) return "...";
      if (error) return "error";

      return data.projets.map(projet => (
        <FactCard
          title={projet.titre}
          description={projet.description}
          date={"aujourd'hui"}
          links={[]}
        />
      ));
    }}
  </Query>
);

export default Home;
