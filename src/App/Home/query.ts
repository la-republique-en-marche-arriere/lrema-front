import gql from 'graphql-tag'

export default gql`
  query {
    projets {
      titre
      description
      liens { titre, source, url }
    }
  }
`
