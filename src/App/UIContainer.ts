import styled from "styled-components";
import { withTheme } from "@material-ui/core";

export const UIContainer = withTheme()(styled.div`
  width: auto;
  margin-left: ${({ theme }) => theme.spacing.unit * 3};
  margin-right: ${({ theme }) => theme.spacing.unit * 3};

  @media (min-width: ${({ theme }) => 1100 + theme.spacing.unit * 3 * 2}) {
    width: 1100px;
    margin-left: auto;
    margin-right: auto;
  }
`);

export default UIContainer;
