import React from "react";
import styled from "styled-components";
import {
  CssBaseline,
  Toolbar as MuiToolbar,
  Typography,
  withTheme
} from "@material-ui/core";
import UIContainer from "./UIContainer";
import Main from "./Main";
import Home from "./Home";

const Toolbar = withTheme()(styled(MuiToolbar)`
  border-bottom: \`1pxsolid${({ theme }) => theme.palette.grey[300]}\`;
`);

const Title = styled(Typography).attrs(() => ({
  component: "h2",
  variant: "h5",
  color: "inherit",
  align: "center",
  noWrap: true
}))`
  flex: 1;
`;

const App = () => (
  <React.Fragment>
    <CssBaseline />
    <UIContainer>
      <Toolbar>
        <Title>La République en Marche (Arrière) !</Title>
      </Toolbar>
      <Main>
        <Home />
      </Main>
    </UIContainer>
  </React.Fragment>
);

export default App;
