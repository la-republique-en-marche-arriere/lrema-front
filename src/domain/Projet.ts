import { Lien } from "./Lien";

export interface Projet {
  titre: string;
  description: string;
  liens: Lien[];
}
