export interface Lien {
  titre: string;
  source: string;
  url: string;
}
