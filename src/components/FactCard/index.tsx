import React from "react";
import MuiCard from "@material-ui/core/Card";
import { CardHeader } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import MuiCardMedia from '@material-ui/core/CardMedia';
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import MuiAvatar from "@material-ui/core/Avatar";
import red from "@material-ui/core/colors/red";
import { Lien } from "../../domain/Lien";

const Card = styled(MuiCard)`
  margin: 30px;
  min-width: 275px;
  max-width: 400px;

`;

const CardMedia = styled(MuiCardMedia)`
    background-size:cover;
    padding-top: 56.25%;
`;

const Avatar = styled(MuiAvatar).attrs(() => ({ "aria-label": "Bot" }))`
  background-color: ${red[500]};
`;

const imageUrl = "https://via.placeholder.com/1920x1080/0000FF/FFFFFF";

interface Props {
  title: string;
  description: string;
  date: string;
  links: Lien[];
}

const FactCard = ({ title, description, date, links }: Props) => (
  <Card>
    <CardHeader avatar={<Avatar>EM</Avatar>} title={title} subheader={date} />
        <CardMedia title={title} image={imageUrl} />
    <CardContent>
      <Typography component="p">{description}</Typography>
    </CardContent>
    <CardActions>
      <Button size="small">Liens</Button>
    </CardActions>
  </Card>
);

export default FactCard;
